#include<stdio.h>
#include<string.h>
#include<emscripten/emscripten.h>

const char alphabet[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q' ,'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

const int alph_len = 52;

int main(int argc, char** argv)
{
    return 0;
}

char encode_char(char elem , int shift)
{
    for (int i = 0; i < alph_len; i++)
    {
       if(elem == alphabet[i]){
           if(i + shift >= 52){
               elem = alphabet[(i + shift) - 52];
               return elem;
           }
            else{
               elem = alphabet[ i + shift ];
               return elem;
           }
       }
    }
    return elem;  
}

char* EMSCRIPTEN_KEEPALIVE encode_msg(char * msg, int shift)
{
    int len  = strlen(msg);
    char temp_msg[len];
    temp_msg[0] = '\0';
    strcpy(temp_msg, msg);
    for(int i=0; i < len; i++)
    {
      msg[i] = encode_char(temp_msg[i], shift); 
    }
    return msg;
}

char decode_char(char elem , int shift)
{
    for (int i = 0; i < alph_len; i++)
    {
       if(elem == alphabet[i]){
           if(i - shift <= 0){
               elem = alphabet[52 - (i + shift)];
               return elem;
           }
            else{
               elem = alphabet[ i - shift ];
               return elem;
           }
       }
    }
    return elem;  
}

char* EMSCRIPTEN_KEEPALIVE decode_msg(char * msg, int shift)
{
    int len  = strlen(msg);
    char temp_msg[len];
    temp_msg[0] = '\0';
    strcpy(temp_msg, msg);
    for(int i=0; i < len; i++)
    {
      msg[i] = decode_char(temp_msg[i], shift); 
    }
    return msg;
}


